<div class="panel-info-kendaraan pt-3 pb-3">
	<h4 class="primary-color text-center">Informasi Kendaraan Bermotor</h4>
	@if( !empty($data) )
		<h6 class="text-center">Tanda Nomor Kendaraan Bermotor</h6>
		<h3 class="text-center primary-color">{{ $data->plat_number }}</h3>
		<div class="mx-auto">
			<img id="barcode" class="mx-auto d-block"/>
		</div>
		<h6 class="text-center mt-3">Masuk: {{date("d-m-Y H:i:s", strtotime($data->checkin_at))}}</h6>
		
	@endif
</div>

