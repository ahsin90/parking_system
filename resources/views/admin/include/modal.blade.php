<!-- Modal MD -->
<div class="modal fade" id="ahsModalMD" tabindex="-1" role="dialog" aria-labelledby="ahsModal" aria-hidden="true" style="background: rgba(0, 0, 0, 0.6) none repeat scroll 0% 0%;">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">

      <div class="modal-body" id="modal_md_content">
       
      </div>
      
    </div>
  </div>
</div>
<!-- ./modal -->