@extends('admin.layout.main')


@section('title', 'Kendaraan Masuk')

@section('breadcrumb')
<!-- BEGIN BREADCRUMB -->
<div class="col-12 mb-3">
	<nav class="widget breadcrumb-two" aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item active">
				<a href="">Kendaraan Masuk</a>
			</li>
			<li class="breadcrumb-item"><a href="javascript:void(0);">&nbsp;</a></li>
		</ol>
	</nav>
</div>
<!-- ./END BREADCRUMB -->
@endsection

@section('content')
	<div class="row">
		<div class="col-md-6">
			<p>Masukkan Tanda Nomor Kendaraan Bermotor</p>
			<form method="POST" action="{{ route('kendaraan_masuk.kendaraanMasukPost') }}" id="vehicleInputForm" onsubmit="vehicleInput(event)">
              	@csrf
              	<!-- Input Box -->
			    <div class="input-group form-container">
			      <input type="text" name="plat_number" class="form-control search-input" placeholder="Ex. B1234TQ" autofocus="autofocus" autocomplete="off">
			          
		          <span class="input-group-btn">
		            <button class="btn btn-search">
		              <img src="{{ url('assets/images/icons/search.png') }}" width="40">
		            </button>
		          </span>

			    </div>
      			<!-- ./End of input box -->
            </form>
            

            <div class="alert custom-alert-1 mb-4 mt-3" role="alert" id="errorMessageBox" style="display: none;">
                <div class="media">
                    <div class="alert-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-triangle"><path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path><line x1="12" y1="9" x2="12" y2="13"></line><line x1="12" y1="17" x2="12" y2="17"></line></svg>
                    </div>

                    <div class="media-body">
                        <div class="alert-text" id="errorMessage"></div>
                    </div>

                </div>
            </div>

		</div>

		<div class="col-md-6" id="vehicleInfo">
			
		</div>

		<div class="col-md-12 mt-5">
			<h3 class="primary-color">Kendaraan di area parkir</h3>
			<div class="table-responsive">
	            <table class="table table-hover" id="datatable-grid">
	                <thead>
	                    <tr>
	                    	<th>Nomor Polisi</th>
	                        <th>Kode</th>
	                        <th>Waktu Masuk</th>
	                    </tr>
	                </thead>
	                
	            </table>
	        </div>
		</div>
	</div>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/jsbarcode/3.3.20/JsBarcode.all.min.js"></script>

<script type="text/javascript">
	
	$(document).ready(function() {
        var dataTable = $('#datatable-grid').DataTable( {
			drawCallback: function () {
			    $('[data-toggle="tooltip"]').tooltip();
			},
			processing    : false,
			serverSide    : true,
			bLengthChange : true,
			"oLanguage": {
				"sProcessing": "loading...",
                "oPaginate": { 
                	"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' 
                },

            	"sInfo": "Menampilkan halaman _PAGE_ of _PAGES_, total rekod _TOTAL_",
            	"sInfoEmpty": "Menampilkan halaman _PAGE_ of _PAGES_, total rekod _TOTAL_",
            
            	"sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            
            	"sSearchPlaceholder": "Cari...",

           		"sLengthMenu": "Filter :  _MENU_",

           		"sEmptyTable": "Tidak ada data untuk ditampilkan"
            },
            "stripeClasses": [],
            
			ajax :{
			    url :"{{ route('kendaraan_masuk.kendaraanTersediaJson') }}", // json datasource
			    type: "get",  // method  , by default get
			    error: function(request, status, error){  // error handling
			        
			        console.log(request.responseText);
			        
			    }
			},
			columns: [
				{data: 'plat_number'},
				{data: 'code'},
	            {data: 'checkin_at'},
	        ]

		});
    });

	function vehicleInput(event){
		event.preventDefault();

		var data = new FormData($("#vehicleInputForm")[0]);
	    var action  = $('#vehicleInputForm').attr('action');

	    $.ajax({
	        'type': 'POST',
	        'url': action,
	        dataType: 'json',
	        'data': data,
	        'processData': false,
	        'contentType': false,
	        'cache': false,

	        beforeSend: function(){
	            $("#loader").show();
	            $("#errorMessage").html("");
	        },
	        success: function(data) { 
	            if(data.success){
	            	$('#datatable-grid').DataTable().ajax.reload();
	                getVehicleInfo(data.data)
	            }else{
	               	
	                $.each(data.errors, function (key, val) {
	                    console.log(val)
	                
	                    $("#errorMessage").append(val[0]);

	                    $("#errorMessageBox").show();

	                    setTimeout(() => {
	                    	$("#errorMessageBox").hide();
	                    }, 4000);

	                });

	                $("html, body").animate({ 
	                    scrollTop: 0 
	                }, 600);
	                
	            }             
	            
	        },error: function(request, status, error){
	           console.log(request.responseText);
	        },complete: function(){
	            $("#loader").hide();
	        }
	    }); 
	}

	function getVehicleInfo(code){
	    $.ajax({
	        url: "{{ route('kendaraan_masuk.infoKendaraanCode') }}",
	        type: "post",
	        data: {code: code, "_token": $('meta[name="csrf_token"]').attr('content') },
	        cache: false,
	        beforeSend: function(){
	            $("#loader").show();
	        },
	        success: function(res) { 
	        	$("#vehicleInfo").html(res);

	        	$("#barcode").JsBarcode(code);

	        },error: function(request, status, error){
	           console.log(request.responseText);
	        },complete: function(){
	            $("#loader").hide();
	        }
	    });
		
	}
</script>
@endsection

