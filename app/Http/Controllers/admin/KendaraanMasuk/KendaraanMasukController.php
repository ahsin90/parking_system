<?php

namespace App\Http\Controllers\admin\KendaraanMasuk;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Vehicle;
use DataTables;
use Carbon\Carbon;

class KendaraanMasukController extends Controller
{
    private $mod_path = 'admin/kendaraan_masuk';

    function index(){
        if( access($this->mod_path, 'read') === false){
            die("Access denied");
        }

        return view('admin.pages.kendaraan_masuk.index');
    }

    function kendaraanMasukPost(Request $request){
        if( access($this->mod_path, 'create') === false){
            die("Access denied");
        }

        $rules['plat_number'] = 'required';


        $messages =[
              'plat_number.required'=>'Anda belum memasukkan tanda nomor kendaraan bermotor'
          ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()){
            return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ));
        }else{

            if( $this->isVehicleAvailable($request->plat_number) ){
                return response()->json(array(
                    'success'   => false,
                    'errors'    => array( ['Kendaraan masih berada dalam area parkir!'] )
                    )
                );
            }else{
                $vehicle = new Vehicle;
                $vehicle->plat_number  = preg_replace('/\s+/', '', $request->plat_number);
                $vehicle->code         = generateNumber(8);
                $vehicle->checkin_at   = now();
                $vehicle->created_at   = now();
                $vehicle->updated_at   = now();

                if( $vehicle->save() ){
                    return response()->json(array(
                        'success' => true,
                        'data'    => $vehicle->code
                    ), 200);
                }
            }
            
        }
    }

    function isVehicleAvailable($plat_number){
        if( access($this->mod_path, 'create') === false){
            die("Access denied");
        }

        $count = Vehicle::where('plat_number', $plat_number)->whereNull('checkout_at')->count();

        if( $count > 0 ){
            return true;
        }else{
            return false;
        }  
    }

    function infoKendaraanByCode(Request $request){
        $code = $request->code;

        $q = Vehicle::where('code', $code);
        if( $q->count() == 1 ){
            $data = $q->first();
        }else{
            $data = null;
        }

        return view('admin.pages.kendaraan_masuk.info', ['data' => $data])->render();
    }

    function kendaraanTersediaJson(Request $request){

        if( access($this->mod_path, 'create') === false){
            die("Access denied");
        }

        if ($request->ajax()) {
            $data = Vehicle::whereNull('checkout_at')->latest()->get();

            return Datatables::of($data)
                ->addIndexColumn()

                ->editColumn('checkin_at', function ($request) {
                    // return Carbon::createFromFormat('Y-m-d H:i:s', $request->checkin_at)->toDateTimeString();
                    return date("d-m-Y H:i:s", strtotime($request->checkin_at));
                })
                ->rawColumns(['nopol'])
                ->make(true);
        }
    }
}
