@extends('admin.layout.main')


@section('title', 'Role')

@section('breadcrumb')
<!-- BEGIN BREADCRUMB -->
<div class="col-12 mb-3">
	<nav class="widget breadcrumb-two" aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="">ACL</a>
			</li>
			<li class="breadcrumb-item active">
				<a href="">Role</a>
			</li>
			<li class="breadcrumb-item"><a href="javascript:void(0);">&nbsp;</a></li>
		</ol>
	</nav>
</div>
<!-- ./END BREADCRUMB -->
@endsection

@section('content')
<div class="panel-body mt-2">

        <div class="table-responsive">
            <table class="table table-hover" id="datatable-grid">
                <thead>
                    <tr>
                    	<th>Name</th>
                    	<th>Dibuat</th>
                    	<th>Update Terakhir</th>
                        <th>&nbsp; &nbsp; &nbsp;&nbsp; Aksi &nbsp;&nbsp; &nbsp;</th>
                    </tr>
                </thead>
                
            </table>
        </div>
</div>
@endsection


@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $('#datatable-grid').DataTable( {
			drawCallback: function () {
			    $('[data-toggle="tooltip"]').tooltip();
			},
			processing    : false,
			serverSide    : true,
			bLengthChange : true,
			"oLanguage": {
				"sProcessing": "loading...",
                "oPaginate": { 
                	"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' 
                },

            	"sInfo": "Menampilkan halaman _PAGE_ of _PAGES_, total rekod _TOTAL_",
            	"sInfoEmpty": "Menampilkan halaman _PAGE_ of _PAGES_, total rekod _TOTAL_",
            
            	"sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            
            	"sSearchPlaceholder": "Cari...",

           		"sLengthMenu": "Filter :  _MENU_",

           		"sEmptyTable": "Tidak ada data untuk ditampilkan"
            },
            "stripeClasses": [],
            
			ajax :{
			    url :"{{ route('acl.role.roleJson') }}", // json datasource
			    type: "get",  // method  , by default get
			    error: function(request, status, error){  // error handling
			        
			        console.log(request.responseText);
			        
			    }
			},
			columns: [
				{data: 'name', name: 'name'},
				{data: 'created_at', name: 'created_at'},
				{data: 'updated_at', name: 'updated_at'},

	            {
	                data: 'action', 
	                name: 'action', 
	                orderable: true, 
	                searchable: true
	            },
	        ]

		});
        
    });

</script>
@endsection