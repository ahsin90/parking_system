<?php 

use Illuminate\Support\Facades\DB;


if ( ! function_exists('settings'))
{
    function settings($key)
    {
       $q = DB::table('settings')
            ->where('setting_key', $key)
            ->first();

        return $q->setting_value;
    }
}

if ( ! function_exists('generateNumber'))
{
    function generateNumber($length)
    {
        $number = '';
        do {
            for ($i=$length; $i--; $i>0) {
                $number .= mt_rand(0,9);
            }
        } while ( !empty(DB::table('vehicles')->where('code', $number)->first(['code'])) );

        return $number;
    }
}

if ( ! function_exists('rupiah'))
{
    function rupiah($num)
    {
        return number_format($num,0,',','.');
    }
}


