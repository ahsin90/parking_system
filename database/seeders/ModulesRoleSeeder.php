<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModulesRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $qm = DB::table('modules')->get();

        $data = array();
        foreach( $qm as $m ){
            // administrator
            $data[] = array(
                'role_id'          =>1,
                'module_id'    => $m->id,
                'create'        => 1,
                'read'        => 1,
                'update'        => 1,
                'delete'        => 1,
                'created_at'    => now(),
                'updated_at'    => now(),
            );

            
            if( $m->path == 'admin/kendaraan_masuk' ){
                // petugas parkir
                $data[] = array(
                    'role_id'      =>2,
                    'module_id'    => $m->id,
                    'create'        => 1,
                    'read'        => 1,
                    'update'        => 1,
                    'delete'        => 1,
                    'created_at'   => now(),
                    'updated_at'   => now(),
                );
            }

            if( $m->path == 'admin/kendaraan_keluar' ){
                // petugas parkir
                $data[] = array(
                    'role_id'      => 2,
                    'module_id'    => $m->id,
                    'create'        => 1,
                    'read'        => 1,
                    'update'        => 1,
                    'delete'        => 1,
                    'created_at'   => now(),
                    'updated_at'   => now(),
                );
            }


        }
        DB::table('modules_role')->insert($data);
    }
}
