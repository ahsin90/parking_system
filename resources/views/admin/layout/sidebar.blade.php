BEGIN MAIN CONTAINER  -->
<div class="main-container sidebar-closed sbar-open" id="container">

<div class="overlay"></div>
<div class="cs-overlay"></div>
<div class="search-overlay"></div>

<!--  BEGIN SIDEBAR  -->
<div class="sidebar-wrapper sidebar-theme">
    
    <nav id="sidebar">

        <ul class="navbar-nav theme-brand flex-row  text-center">
            <li class="nav-item theme-logo">
                <a href="{{ route('dashboard.index') }}">
                    <img src="{!! url('assets/images/icons/car.png') !!}" class="navbar-logo" alt="logo">
                </a>
            </li>
            <li class="nav-item theme-text">
                <a href="{{ route('dashboard.index') }}" class="nav-link"> parking</a>
            </li>
        </ul>

        <ul class="list-unstyled menu-categories" id="accordionExample">
            <!-- Menus without child -->
            <li class="menu">
                <a href="{{ route('dashboard.index') }}" aria-expanded="true" class="dropdown-toggle">
                    <div class="">
                        
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                        
                        <span>Dashboard</span>
                    </div>
                </a>
            </li>
            <!-- ./ Menus -->

            @foreach( getModules( auth()->user()->role_id ) as $key => $val )

                @if( empty($val['parent_id']) && !empty($val['path']))
                <!-- Menu without child -->
                <li class="menu">
                    <a href="{!! url($val['path']) !!}" aria-expanded="true" class="dropdown-toggle">
                        <div class="">
                            @if($val['icon'])
                                {!! $val['icon'] !!}
                            @else
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-disc"><circle cx="12" cy="12" r="10"></circle><circle cx="12" cy="12" r="3"></circle></svg>
                            @endif
                            <span>{{ $val['name'] }}</span>
                        </div>
                    </a>
                </li>
                <!-- ./ Menu without child -->
                @endif


                @if( empty($val['parent_id']) && empty($val['path']))
                <!-- Parent menus -->
                <li class="menu">
                    <a href="#menu{{$val['id']}}" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">
                        <div class="">
                            
                            @if($val['icon'])
                                {!! $val['icon'] !!}
                            @else
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-disc"><circle cx="12" cy="12" r="10"></circle><circle cx="12" cy="12" r="3"></circle></svg>
                            @endif
                            <span>{{ $val['name'] }}</span>
                        </div>
                        
                        <div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                        </div>
                    </a>
                </li>
                <!-- ./Parent menus -->
                @endif

                @if( !empty($val['child']) )

                    @foreach($val['child'] as $child)
                        <!-- child menus -->
                        <ul class="collapse submenu list-unstyled" id="menu{{$val['id']}}" data-parent="#accordionExample">
                            <li>
                                <a href="{{ url($child['path']) }}">- {{ $child['name'] }}</a>
                            </li>
                        </ul>
                        <!-- ./ child menus -->
                    @endforeach

                @endif


                
                
            @endforeach

        </ul>
        
    </nav>

</div>
<!-- END SIDEBAR  