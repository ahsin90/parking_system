<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // admin
        DB::table('users')->insert([
            'name'          => 'Muhammad Ahsin',
            'email'         => 'ahsin.and@gmail.com',
            'username'      => 'admin',
            'password'      => bcrypt('admin'),
            'role_id'       => 1, //administrator
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ]);

        // petugas
        DB::table('users')->insert([
            'name'          => 'Petugas Parkir',
            'email'         => 'petugas@mail.com',
            'username'      => 'petugas',
            'password'      => bcrypt('petugas'),
            'role_id'       => 2, //administrator
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ]);
    }
}
