function load_form(target, id = ''){
    $.ajax({
        url: target,
        type: "post",
        data: {id: id},
        cache: false,
        beforeSend: function(){
            $("#loader").show();
        },
        success: function(data) {            
            $('#modal_content').html(data);       
            
        },error: function(request, status, error){
           console.log(request.responseText);
        },complete: function(){
            $("#loader").hide();
        }
    });
}

function load_form2(target, id = ''){
    $("#loader").show();
    $.ajax({
        url: target,
        type: "post",
        data: {id: id, "_token": $('meta[name="csrf_token"]').attr('content') },
        cache: false,
        beforeSend: function(){
            $("#loader").show();
        },
        success: function(data) { 

            $('#modal_md_content').html(data);

            // var year = new Date();
            // var next_year = year.getFullYear()+5;

            // $( ".datepicker" ).datepicker({
            //   changeMonth: true,
            //   changeYear: true,
            //   dateFormat: 'dd-mm-yy',
            //   yearRange: 'now():'+next_year
            // }); 

            // $(".select2").select2();
            
        },error: function(request, status, error){
           console.log(request.responseText);
        },complete: function(){
            $("#loader").hide();
        }
    });
}

function save(event, after = null, success_message = null){
    event.preventDefault();
    var data = new FormData($("#save")[0]);
    var action  = $('#save').attr('action');

    var after_arr = [];
    if( after ){
        after_arr = after.split('|');
    }  

    if( success_message != null ){
        success_message = success_message;
    }else{
        success_message = 'Data Telah Tersimpan';
    }

    $.ajax({
        'type': 'POST',
        'url': action,
        dataType: 'json',
        'data': data,
        'processData': false,
        'contentType': false,
        'cache': false,

        beforeSend: function(){
            $("#loader").show();
        },
        success: function(data) { 
            if(data.success){
                swal({title: "Berhasil!", text: success_message, type: "success"}, () =>{ 
                    if( after_arr.includes('reload', after_arr) ){
                        // Reload page
                        location.reload();
                    }
                    if( after_arr.includes('dt_reload', after_arr) ){
                        // Reload datatable
                        $('#datatable-grid').DataTable().ajax.reload();
                    }
                    if( after_arr.includes('dismissModalMD', after_arr) ){
                        // Dismiss ahsModalMD
                        $('#ahsModalMD').modal('toggle');
                    }
                    if( after_arr.includes('dismissModal', after_arr) ){
                        // Dismiss ahsModal
                        $('#ahsModal').modal('toggle');
                    }
                    if( after_arr.includes('redirect', after_arr) ){
                        // Redirect to page by data response
                        window.location.replace(data.success);
                    }

                });

            }else{
                // $("#errorMessage").html("");

                console.log(data.errors)

                $.each(data.errors, function (key, val) {
                    console.log(val)
                
                    $("#errorMessage").append("<div class='alert alert-danger mb-1 p-2'>"+val[0]+"</div>")

                });

                $("html, body").animate({ 
                    scrollTop: 0 
                }, 600);
                
            }             
            
        },error: function(request, status, error){
           console.log(request.responseText);
        },complete: function(){
            $("#loader").hide();
        }
    }); 
}


function hapus(action, id, after = ''){
    var after_arr = after.split('|');

    swal({
      title: "Apakah anda yakin menghapus data?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Setuju!",
      closeOnConfirm: false,
      html: true
    }, function(){

        $.ajax({
            type: 'DELETE',
            dataType: 'json',
            url: action,
            data: {id: id, "_token": $('meta[name="csrf_token"]').attr('content')},
            cache: false, 

            beforeSend: function(){
                $("#loader").show();
            },
            success: function(data_r) { 
                if( data_r.success ){
                    swal({title: "Berhasil!", text: 'Data Berhasil di Hapus', type: "success"}, () =>{
                        if( after_arr.includes('reload', after_arr) ){
                            // Reload page
                            location.reload();
                        }
                        if( after_arr.includes('dt_reload', after_arr) ){
                            // Reload datatable
                            $('#datatable-grid').DataTable().ajax.reload();
                        }
                        if( after_arr.includes('dismissModalMD', after_arr) ){
                            // Dismiss ahsModalMD
                            $('#ahsModalMD').modal('toggle');
                        }
                        if( after_arr.includes('dismissModal', after_arr) ){
                            // Dismiss ahsModal
                            $('#ahsModal').modal('toggle');
                        }
                        if( after_arr.includes('redirect', after_arr) ){
                            // Redirect to page by data response
                            window.location.replace(data_r.success);
                        }
                    });
                }else{
                    swal({title: "Gagal!", text: 'Gagal Terhapus', type: "error"});
                } 
            },error: function(request, status, error){
              console.log(request.responseText);
            },complete: function(){
                $("#loader").hide();
            }
        });

    });
}

function cari_mahasiswa(event){
    event.preventDefault();
    var data = new FormData($("#searchOnNavbar")[0]);
    var action  = $('#searchOnNavbar').attr('action');
    var keyword = $("#InputSearchOnNavbar").val();

    if(keyword){
        $.ajax({
            url: action,
            type: "post",
            data: {keyword: keyword},
            cache: false,
            beforeSend: function(){
                $("#loader").show();
            },
            success: function(data) {  
                // display modal
                $('#ahsModalMD').modal('show');
                $('#modal_md_content').html(data);
            },error: function(request, status, error){
               console.log(request.responseText);
            },complete: function(){
                $("#loader").hide();
            }
        });
    }
    
}