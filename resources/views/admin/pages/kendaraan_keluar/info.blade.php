<div class="panel-info-kendaraan pt-3 pb-3">
	<h4 class="primary-color text-center">Informasi Kendaraan Bermotor</h4>
	@if( !empty($data) )
		<h6 class="text-center">Tanda Nomor Kendaraan Bermotor</h6>
		<h3 class="text-center primary-color">{{ $data->plat_number }}</h3>
		
		<h6 class="text-center mt-3">Masuk: {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data->checkin_at)->toDateTimeString() }}</h6>

		<div class="mt-2">
			<h6 class="text-center">Biaya Parkir Selama {{ $totalHours }} Jam</h6>
			<h1 class="text-center">Rp. {{ $price }}</h1>
		</div>
		
	@endif
</div>

