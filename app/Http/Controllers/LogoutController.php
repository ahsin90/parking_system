<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class LogoutController extends Controller
{
    function index(){
        Session::flush();
        
        Auth::logout();

        return redirect('login');
    }
}
