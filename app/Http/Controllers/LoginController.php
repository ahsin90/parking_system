<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Models\User;

class LoginController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return redirect('dashboard');
        }else{
            return view('login');
        }
    }

    function loginAction(Request $request){
        $credentials = $request->validate([
            'username' => ['required'],
            'password' => ['required'],
        ]);
        
        $response = array();
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            
            // update last login
            $user = User::find( Auth::id() );
            $user->last_login = date('Y-m-d H:i:s');
            $user->save();

            $response = array( 'status' => 'success', 'message' => null );
        }else{
            $response = array( 'status' => 'fail', 'message' => 'Username atau password tidak sesuai' );
        }
 
        // return back()->withErrors([
        //     'username' => 'Username atau password tidak sesuai.',
        // ])->onlyInput('username');

        return response()->json($response);
    }


    // public function username()
    // {
    //     return 'username';
    // }
}
