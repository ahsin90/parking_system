@extends('admin.layout.main')


@section('title', 'Pelaporan')

@section('breadcrumb')
<!-- BEGIN BREADCRUMB -->
<div class="col-12 mb-3">
	<nav class="widget breadcrumb-two" aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item active">
				<a href="">Pelaporan</a>
			</li>
			<li class="breadcrumb-item"><a href="javascript:void(0);">&nbsp;</a></li>
		</ol>
	</nav>
</div>
<!-- ./END BREADCRUMB -->
@endsection

@section('content')
<!-- filter form -->
<div class="col-md-12">
	<form enctype="multipart/form-data" accept-charset="utf-8" method="GET" action="">
	    <div class="row pt-4 pb-4" style="background: #f7f7f7;">
	        <div class="col-md-12">
	            <div class="row">
	                <div class="col-md-3 form-group">
	                	<input type="text" name="start_date" value="{{ request()->start_date }}" class="form-control flatpickr" placeholder="Tanggal Awal" required />
	                </div>
	                <div class="col-md-3">
	                    <input type="text" name="end_date" value="{{ request()->end_date }}" class="form-control flatpickr" placeholder="Tanggal Akhir" required />
	                </div>
	                <div class="col-md-2">
	                    <button type="submit" class="btn btn-primary btn-lg btn-block">Tampilkan</button>
	                </div>
	            </div>
	        </div>
	    </div>
	</form>
</div>
<!-- filter form -->


<!-- Table -->
<div class="col-md-12 mt-2">
	<button class="btn btn-primary" onclick="downloadExcel(event, '{{$downloadUrl}}')">Download Excel</button>

	<div class="table-responsive">
        <table class="table table-hover" id="datatable-grid">
            <thead>
                <tr>
                	<th>Tanggal</th>
                    <th>Jumlah Kendaraan</th>
                    <th>Jumlah Harga</th>
                </tr>
            </thead>
            
        </table>
    </div>
</div>
<!-- Table -->
@endsection

@section('script')
<script>
	$(document).ready(function() {
	    var dataTable = $('#datatable-grid').DataTable( {
			drawCallback: function () {
			    $('[data-toggle="tooltip"]').tooltip();
			},
			processing    : false,
			serverSide    : true,
			bLengthChange : true,
			"oLanguage": {
				"sProcessing": "loading...",
	            "oPaginate": { 
	            	"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' 
	            },

	        	"sInfo": "Menampilkan halaman _PAGE_ of _PAGES_, total rekod _TOTAL_",
	        	"sInfoEmpty": "Menampilkan halaman _PAGE_ of _PAGES_, total rekod _TOTAL_",
	        
	        	"sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
	        
	        	"sSearchPlaceholder": "Cari...",

	       		"sLengthMenu": "Filter :  _MENU_",

	       		"sEmptyTable": "Tidak ada data untuk ditampilkan"
	        },
	        "stripeClasses": [],
	        
			ajax :{
			    url :"{{ route('pelaporan.pelaporanJson') }}", // json datasource
			    type: "get",  // method  , by default get
			    data: {
			    	'start_date': getUrlParameter('start_date'),
			    	'end_date': getUrlParameter('end_date')
			    },
			    error: function(request, status, error){  // error handling
			        
			        console.log(request.responseText);
			        
			    }
			},
			columns: [
				{data: 'date'},
				{data: 'total_vehicle'},
				{data: 'total_price'},
	        ]

		});
	})

	var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = window.location.search.substring(1),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
	        }
	    }
	    return false;
	};

	function downloadExcel(event, urlTarget){
		event.preventDefault();

		let startDate 	= getUrlParameter('start_date');
		let endDate 	= getUrlParameter('end_date');


		if( startDate != '' && endDate != '' ){

			fetch(urlTarget)
			.then(resp => resp.blob())
			.then(blob => {
				const url = window.URL.createObjectURL(blob);
				const a = document.createElement('a');
				a.style.display = 'none';
				a.href = url;
				// the filename you want
				a.download = 'Pelaporan.xlsx';
				document.body.appendChild(a);
				a.click();
				window.URL.revokeObjectURL(url);
			})
			.catch(() => alert('download error'));

		}else{
			alert("silahkan pilih tanggal")
		}
	}
</script>
@endsection

