<h3 class="primary-color">{{ @$title }}</h3>

<div id="errorMessage"></div>

 <form role="form" enctype="multipart/form-data" accept-charset="utf-8" method="post" action="{{ @$postRoute }}" id="save" onsubmit="save(event, `dt_reload|dismissModalMD`);">
  @csrf
  
  <div class="form-row row">
      <input type="hidden" name="id" value="{{ @$data->id }}">
      <div class="form-group col-md-6">
          <label>Username<span class="text-danger">*</span></label><input type="text" name="username" value="{{ @$data->username }}" class="form-control" placeholder="Username" required />
      </div>
      <div class="form-group col-md-6">
          <label>Email<span class="text-danger">*</span></label><input type="text" name="email" value="{{ @$data->email }}" class="form-control" placeholder="Email" required />
      </div>

      <div class="form-group col-md-12">
          <label>Nama<span class="text-danger">*</span></label><input type="text" name="full_name" value="{{ @$data->name }}" class="form-control" placeholder="Nama"  />
      </div>

      <div class="form-group col-md-12">
          <label>Role<span class="text-danger">*</span></label><br />

          @foreach(getMasterRoles() as $role)

            <label class="custom-radio-button"><input type="radio" name="role" value="{{$role->id}}" {{ ($role->id==@$data->role_id)?"checked":"" }}/><span class="helping-el"></span><span class="label-text">{{$role->name}} </span></label>

          @endforeach

      </div>

      <div class="form-group col-md-6">
          <label>Password</label><input type="password" name="password" class="form-control" placeholder="Password" />
      </div>

      <div class="form-group col-md-6">
          <label>Verifikasi Password</label><input type="password" name="password2" class="form-control" placeholder="Password" />
      </div>
      
      <button type="submit" class="btn btn-primary">Simpan</button>
  </div>
</form>

</div>