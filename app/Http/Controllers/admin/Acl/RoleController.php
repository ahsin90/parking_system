<?php

namespace App\Http\Controllers\admin\Acl;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Roles;
use DataTables;

class RoleController extends Controller
{
    private $mod_path = 'admin/acl/role';

    function index(){
        if( access($this->mod_path, 'read') === false){
            die("Access denied");
        }

        return view('admin.pages.acl.role');
    }

    function roleJson(Request $request){
        if( access($this->mod_path, 'read') === false){
            die("Access denied");
        }
        
        if ($request->ajax()) {
            $data = Roles::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('created_at', function ($request) {
                    return $request->created_at->format('Y-m-d H:i:s');
                })
                ->editColumn('updated_at', function ($request) {
                    return $request->updated_at->format('Y-m-d H:i:s');
                })
                ->addColumn('action', function($row){
                    $actionBtn = '';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
}
