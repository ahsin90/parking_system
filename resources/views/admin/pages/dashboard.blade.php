@extends('admin.layout.main')


@section('title', 'Dashboard')

@section('breadcrumb')
<!-- BEGIN BREADCRUMB -->
<div class="col-12 mb-3">
	<nav class="widget breadcrumb-two" aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item active">
				<a href="">Dashboard</a>
			</li>
			<li class="breadcrumb-item"><a href="javascript:void(0);">&nbsp;</a></li>
		</ol>
	</nav>
</div>
<!-- ./END BREADCRUMB -->
@endsection

@section('content')

<div class="col-12 layout-spacing">
    <div class="row dashboard-card mt-3 bg-white dashboard-card-primary">
        <div class="col-md-8 my-auto">
            <div class="d-flex">
                <div class="wrapper">
                    <h2 class="page-title primary-color">Halo, {{ auth()->user()->name }}</h2>
                    <h6 class="pb-3 text-muted">Selamat datang :)</h6>
                </div>
            </div>
        </div>

        <div class="col-md-4 my-auto">
    		<div class="d-flex">
    			<div class="wrapper">
    				<h4 class="mb-0 text-muted digital-clock-day text-warning"></h4>
    				<h3 class="mb-0 text-muted digital-clock-date"></h3>
    				<h5 class="mb-0 text-muted digital-clock-time"></h5>
    			</div>
    		</div>
    	</div>
    </div>
</div>

@if( auth()->user()->role_id == 1)
<div class="row">
    <div class="col-md-12">
        <div class="widget widget-one">
            <h3 class="primary-color">Grafik Kendaraan</h3>
            <canvas id="chart"></canvas>
        </div>
    </div>

    <div class="col-md-12 mt-4">
        <div class="widget widget-one">
            <h3 class="primary-color">Grafik Pendapatan Harian</h3>
            <canvas id="chart2"></canvas>
        </div>
    </div>
</div>

@endif

@endsection

@section('script')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>

    <script type="text/javascript">
        
        var barChartData = {
          labels: {!! $checkinLabel !!},
          datasets: [
            {
              label: "Masuk",
              backgroundColor: "#0F515B",
              borderColor: "#0F515B",
              borderWidth: 1,
              data: {!! $checkinData !!}
            },
            {
              label: "Keluar",
              backgroundColor: "#ea1558",
              borderColor: "#ea1558",
              borderWidth: 1,
              data: {!! $checkoutData !!}
            }
          ]
        };

        var chartOptions = {
          responsive: true,
          legend: {
            position: "top"
          },
          title: {
            display: true,
            text: "Jumlah Kendaraan"
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }

   
    var incomeData = {
          labels: {!! $incomeLabel !!},
          datasets: [
            {
              label: "Pendapatan",
              borderColor: "#0F515B",
              borderWidth: 1,
              data: {!! $incomeData !!}
            }
          ]
        };

        var incomeOptions = {
          responsive: true,
          legend: {
            position: "top"
          },
          title: {
            display: true,
            text: "Pendapatan"
          },
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }

    window.onload = function() {
    var ctx = document.getElementById("chart").getContext("2d");
      window.myBar = new Chart(ctx, {
        type: "bar",
        data: barChartData,
        options: chartOptions
      });

      var ctx = document.getElementById("chart2").getContext("2d");
      window.myBar = new Chart(ctx, {
        type: "line",
        data: incomeData,
        options: incomeOptions
      });

    };


    </script>
@endsection


