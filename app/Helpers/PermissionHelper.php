<?php 

use App\Models\Modules;
use App\Models\Roles;
use Illuminate\Support\Facades\DB;
use Auth as User;

if ( ! function_exists('getModules'))
{   function getModules($role_id = '')
    {
    	$q = DB::table('modules')
        	->where('is_active', 1)
        	->where('modules_role.role_id', $role_id)
        	->leftJoin('modules_role', 'modules_role.module_id', '=', 'modules.id')
        	->orderBy('parent_id', 'asc')
            ->get();

        $data = array();
        // $child_arr = [];
		if( count($q) > 0 ){
			foreach( $q as $key => $val ){

				if( empty($val->parent_id ) ){
					// parent
					$data[$val->id] = array(
						'id'		 => $val->id,
						'parent_id'  => $val->parent_id,
						'name'		 => $val->name,
						'path'		 => $val->path,
						'icon'		 => $val->icon,
						'sort'		 => $val->sort,
						'is_active'	 => $val->is_active
					);
				}

				if( !empty( $val->parent_id) ){
					// child
					$data[$val->parent_id]['child'][$val->id] = array(
						'id'		 => $val->id,
						'parent_id'  => $val->parent_id,
						'name'		 => $val->name,
						'path'		 => $val->path,
						'icon'		 => $val->icon,
						'sort'		 => $val->sort,
						'is_active'	 => $val->is_active
					);
				}

				      		
			}
		}

        return $data;
    }
}

if ( ! function_exists('access'))
{   function access($mod_path, $access)
    {
    	$user = Auth::user();

    	$q = DB::table('modules')
    		->where('modules.path', $mod_path)
    		->where('modules_role.role_id', $user->role_id)
    		->where('modules_role.'.$access, 1)
        	->leftJoin('modules_role', 'modules_role.module_id', '=', 'modules.id')
            ->count();

        if( $q > 0 ){
        	return true;
        }

    	return false;
    	
    }
}

if ( ! function_exists('getMasterRoles'))
{   function getMasterRoles()
    {
    	return Roles::get();
    }
}
