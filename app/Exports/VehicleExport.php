<?php

namespace App\Exports;

use App\Models\Vehicle;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;

class VehicleExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;

    public function __construct($startDate, $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate   = $endDate;
    }


    public function headings(): array
    {
        return [
            'Tanggal',
            'Jumlah Kendaraan',
            'Jumlah Harga'
        ];
    }

    public function map($transaction): array
    {
        return [
            $transaction->date,
            $transaction->total_vehicle,
            $transaction->total_price
        ];
    }

    public function collection()
    {
         $data = Vehicle::whereDate('checkout_at', '>=', $this->startDate)
            ->whereDate('checkout_at', '<=', $this->endDate)
            ->whereNotNull('checkout_at')
            ->selectRaw("SUM(price) as total_price, COUNT(*) total_vehicle, DATE_FORMAT(checkout_at, '%e-%m-%Y') as date")
            ->groupBy('date')
            ->get();

        return $data;
    }
}
