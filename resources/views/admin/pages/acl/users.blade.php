@extends('admin.layout.main')


@section('title', 'Users')

@section('breadcrumb')
<!-- BEGIN BREADCRUMB -->
<div class="col-12 mb-3">
	<nav class="widget breadcrumb-two" aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="">ACL</a>
			</li>
			<li class="breadcrumb-item active">
				<a href="">Users</a>
			</li>
			<li class="breadcrumb-item"><a href="javascript:void(0);">&nbsp;</a></li>
		</ol>
	</nav>
</div>
<!-- ./END BREADCRUMB -->
@endsection

@section('content')
<div class="panel-body mt-2">

		<!-- Button -->
		<div class="col-sm-12 mb-3">

            <a href="javascript:void(0)" onclick="loadAddForm()" data-toggle="modal" data-target="#ahsModalMD" class="btn btn-primary">Tambah 
            	<svg class="svg-inline--fa fa-plus-circle fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm144 276c0 6.6-5.4 12-12 12h-92v92c0 6.6-5.4 12-12 12h-56c-6.6 0-12-5.4-12-12v-92h-92c-6.6 0-12-5.4-12-12v-56c0-6.6 5.4-12 12-12h92v-92c0-6.6 5.4-12 12-12h56c6.6 0 12 5.4 12 12v92h92c6.6 0 12 5.4 12 12v56z"></path>
            	</svg>
           	</a>

        </div>

        <div class="table-responsive">
            <table class="table table-hover" id="datatable-grid">
                <thead>
                    <tr>
                    	<th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>&nbsp; &nbsp; &nbsp;&nbsp; Aksi &nbsp;&nbsp; &nbsp;</th>
                    </tr>
                </thead>
                
            </table>
        </div>
</div>
@endsection


@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		var dataTable = $('#datatable-grid').DataTable( {
			drawCallback: function () {
			    $('[data-toggle="tooltip"]').tooltip();
			},
			processing    : false,
			serverSide    : true,
			bLengthChange : true,
			"oLanguage": {
				"sProcessing": "loading...",
                "oPaginate": { 
                	"sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' 
                },

            	"sInfo": "Menampilkan halaman _PAGE_ of _PAGES_, total rekod _TOTAL_",
            	"sInfoEmpty": "Menampilkan halaman _PAGE_ of _PAGES_, total rekod _TOTAL_",
            
            	"sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            
            	"sSearchPlaceholder": "Cari...",

           		"sLengthMenu": "Filter :  _MENU_",

           		"sEmptyTable": "Tidak ada data untuk ditampilkan"
            },
            "stripeClasses": [],
            
			ajax :{
			    url :"{{ route('acl.users.userJson') }}", // json datasource
			    type: "get",  // method  , by default get
			    error: function(request, status, error){  // error handling
			        
			        console.log(request.responseText);
			        
			    }
			},
			columns: [
				{data: 'name', name: 'name'},
	            {data: 'username', name: 'username'},
	            {data: 'email', name: 'email'},
	            {data: 'created_at', name: 'created_at'},
	            {data: 'updated_at', name: 'updated_at'},
	            {
	                data: 'action', 
	                name: 'action', 
	                orderable: true, 
	                searchable: true
	            },
	        ]

		});
        
    });

    function loadAddForm(){
    	$.ajax({
	        url: "{{ route('acl.users.addform') }}",
	        type: "post",
	        data: {"_token": "{{ csrf_token() }}"},
	        cache: false,
	        beforeSend: function(){
	            $("#loader").show();
	        },
	        success: function(data) {       
	            $('#modal_md_content').html(data);       
	        },error: function(request, status, error){
	           console.log(request.responseText);
	        },complete: function(){
	            $("#loader").hide();
	        }
	    });
    }

</script>
@endsection