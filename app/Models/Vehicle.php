<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;

    protected $fillable = [
        'plat_number',
        'code',
        'checkin_at',
        'checkout_at',
        'price'
    ];

    protected $casts = [
        'checkin_at'    => 'datetime:Y-m-d H:i:s',
        'checkout_at'   => 'datetime:Y-m-d H:i:s'
    ];
}
