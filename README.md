## Aplikasi Sistem Parkir Kendaraan:

 * Aplikasi ini dibuat menggunakan laravel terbaru saat ini, yaitu versi 9.11
 * Gunakan PHP minimal versi 8.0 untuk menjalankan aplikasi ini.
 * Saat development, database menggunakan MySQL 5.6
 * Jalankan perintah composer install dalam direktori project
 * Sebelum menjalankan aplikasi, pastikan konfigurasi MySQL pada .env sudah sesuai, kemudian jalankan perintah php artisan migrate --seed (Data sudah tersedia di dalam seeder)
 * akun administrator adalah:
   username: admin
   password: admin
 * akun petugas adalah:
   username: petugas
   password: petugas