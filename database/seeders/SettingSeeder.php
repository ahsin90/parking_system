<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'setting_key'       => 'app_name',
                'setting_value'     => 'Parking System Management',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ),
            array(
                'setting_key'       => 'price_per_hour',
                'setting_value'     => 3000,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            )
        );

        DB::table('settings')->insert($data);
    }
}
