<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Login">
    <title>Login</title>

    <!-- CSS -->
    <link href="{!! url('assets/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! url('assets/login.css') !!}" rel="stylesheet">

    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,400,700&display=swap" rel="stylesheet">

    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

</head>

<body > 
  <div id="loader"></div>

  <div class="fluid-container mx-auto">
    <div class="login-page">
      <div class="d-flex flex-lg-row flex-column">

        <!-- Card 1 -->
        <div class="card card1">
            <div class="row justify-content-center my-auto">
                <div class="col-md-8 col-10 my-5">

                    <a href="{{ route('login') }}">
                        <img src="{!! url('assets/images/logo.png') !!}" class="mx-auto d-block" width="100">
                    </a>

                    <h3 class="mt-3 mb-0 text-center primary-color">Login</h3>

                    <form method="POST" action="{{ route('login') }}" id="login_form" onsubmit="login(event)">

                      <div id="pesan" class="mt-3"></div>

                      <div class="form-floating mb-3">
                        <input type="text" name="username" class="form-control flat-input" id="floatingInput" placeholder="Username">
                        <label for="floatingInput">Username</label>
                      </div>

                      <div class="form-floating">
                        <input type="password" name="password" class="form-control flat-input" id="floatingPassword" placeholder="Password">
                        <label for="floatingPassword">Password</label>
                      </div>

                      @csrf

                      
                      <button type="submit" class="login-btn" id="login_button">Login</button>
                      <br><br>
                    </form>
                </div>
            </div>

            
            
        </div>
        <!-- End Card 1 -->

        <!-- Card 2 -->
        <div class="card card2">
            <div class="my-auto mx-md-5 px-md-5 right">
                <h1 class="page-title secondary-color">Parking Management System</h1>
                <h3 class="text-white"></h3>
                <ul>
                    <li class="text-white">Aplikasi ini berfungsi untuk melakukan manajemen kendaraan parkir</li>
                    <li class="text-white">Aplikasi akan mencatat nomor polisi, waktu masuk dan keluar kendaraan</li>
                    <li class="text-white">Aplikasi ini memiliki 2 hak akses, yaitu administrator dan petugas parkir</li>
                </ul>
            </div>
        </div>
        <!-- ./End card 2 -->

      </div>
    </div>
  </div>



<!-- Bootstrap JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-popRpmFF9JQgExhfw5tZT4I9/CI5e2QcuUZPOVXb1m7qUmeR2b50u+YFEYe1wgzy" crossorigin="anonymous"></script>

<script type="text/javascript">
  function login(event){
    event.preventDefault();
    var action      = $('#login_form').attr('action');
    var formData    = new FormData($("#login_form")[0]);

    $.ajax({
        url: action,
        type: "post",
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function(){
            $("#login_button").html("Harap Tunggu...");
            $('#pesan').html("<div class='alert alert-secondary' role='alert'>Mencoba untuk login...</div><hr>");
        },
        success: function(res) {
            if( res.status == 'success'){
                $(location).attr('href', "{{ route('dashboard.index') }}");
            }else{
                $('#pesan').html('<div class="alert alert-danger">'+res.message+'</div>');
            }

        },error: function(request, status, error){
           console.log(request.responseText);
        },complete: function(){
            $("#login_button").html("Login");
        }
    });

  }

  $("#loader").hide();
</script>

</body>
</html>