<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{!! url('assets/admin/js/popper.min.js') !!}"></script>
<script src="{!! url('assets/admin/js/bootstrap.min.js') !!}"></script>
<script src="{!! url('assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js') !!}"></script>
<script src="{!! url('assets/plugins/notify.min.js') !!}"></script>
<script src="{!! url('assets/admin/js/dataTables.js') !!}"></script>
<script src="{!! url('assets/admin/js/app.js') !!}"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" integrity="sha512-7VTiy9AhpazBeKQAlhaLRUk+kAMAb8oczljuyJHPsVPWox/QIXDFOnT9DUk1UC8EbnHKRdQowT7sOBe7LAjajQ==" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js" integrity="sha512-LGXaggshOkD/at6PFNcp2V2unf9LzFq6LE+sChH7ceMTDP0g2kn6Vxwgg7wkPP7AAtX+lmPqPdxB47A0Nz0cMQ==" crossorigin="anonymous"></script>

<script src="{!! url('assets/admin/js/app/global.js') !!}"></script>
<script src="{!! url('assets/admin/js/custom.js') !!}"></script>

<!-- END GLOBAL MANDATORY SCRIPTS -->

<script>
    $(document).ready(function() {
        App.init();

        flatpickr('.flatpickr', {
            dateFormat: "d-m-Y"
        });

        moment.locale('ID');
        var tanggal = moment().format('Do MMMM YYYY');
        var hari = moment().format('dddd');

        $('.digital-clock-date').html( tanggal );
        $('.digital-clock-day').html( hari );

        setInterval(function(){ 
            var jam = moment().format('h:mm:ss');
            $('.digital-clock-time').html( jam )
        }, 1000);
    });

    $("#loader").hide();
</script>

</body>
</html>