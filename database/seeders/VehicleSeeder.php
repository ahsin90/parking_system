<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class VehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array();
        for ($i=1; $i <= 500; $i++) { 
            // date("Y-m-d", strtotime("-30 days"))
            $randomDate = date("Y-m-d H:i:s", strtotime('-'.rand(1, 10).' days'));

            $checkout = ( $i > 400 )?$randomDate:null;
            $price    = ( !empty($checkout) )?3000:null;

            $data[] = array(
                'plat_number'   => 'B'.$i.strtoupper(Str::random(2)),
                'code'          =>generateNumber(8),
                'checkin_at'    => $randomDate,
                'checkout_at'   => $checkout,
                'price'         => $price,
                'created_at'    => $randomDate,
                'updated_at'    => $randomDate,
            );
        }

        DB::table('vehicles')->insert($data);
    }
}
