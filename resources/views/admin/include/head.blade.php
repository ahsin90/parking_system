<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf_token" content="{{ csrf_token() }}" />
<link rel="icon" type="image/x-icon" href="{!! url('assets/images/favicon.png') !!}"/>
<link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
<link href="{!! url('assets/admin/css/bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
<link href="{!! url('assets/admin/css/main.css') !!}" rel="stylesheet" type="text/css" />
<link href="{!! url('assets/admin/css/structure.css') !!}" rel="stylesheet" type="text/css" />
<link href="{!! url('assets/admin/css/custom.css') !!}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" integrity="sha512-gOQQLjHRpD3/SEOtalVq50iDn4opLVup2TF8c4QPI3/NmUPNZOk2FG0ihi8oCU/qYEsw4P6nuEZT2lAG0UNYaw==" crossorigin="anonymous" />
<link rel="stylesheet" type="text/css" href="{!! url('assets/admin/css/dataTables.css') !!}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@yield('style')