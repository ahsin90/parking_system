<?php

namespace App\Http\Controllers\admin\Pelaporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Vehicle;
use DataTables;
use Carbon\Carbon;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\VehicleExport;


class PelaporanController extends Controller
{
    private $mod_path = 'admin/pelaporan';

    function index(){
        if( access($this->mod_path, 'read') === false){
            die("Access denied");
        }

        $downloadUrl = url('admin/pelaporan/export/?start_date='.request()->start_date.'&end_date='.request()->end_date);
        
        return view('admin.pages.pelaporan.index', ["downloadUrl" => $downloadUrl]);
    }

    function pelaporanJson(Request $request){

        if( access($this->mod_path, 'read') === false){
            die("Access denied");
        }

        if ($request->ajax()) {

            $startDate  = date("Y-m-d", strtotime($request->start_date));
            $endDate    = date("Y-m-d", strtotime($request->end_date));

            $data = Vehicle::whereDate('checkout_at', '>=', $startDate)
            ->whereDate('checkout_at', '<=', $endDate)
            ->whereNotNull('checkout_at')
            ->selectRaw("SUM(price) as total_price, COUNT(*) total_vehicle, DATE_FORMAT(checkout_at, '%e-%m-%Y') as date")
            ->groupBy('date')
            ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('total_price', function ($request) {
                    return 'Rp. '.rupiah($request->total_price);
                })
                ->rawColumns(['nopol'])
                ->make(true);
        }
    }

    public function exportExcel() 
    {
        if( access($this->mod_path, 'read') === false){
            die("Access denied");
        }

        $startDate  = date("Y-m-d", strtotime(request()->start_date));
        $endDate    = date("Y-m-d", strtotime(request()->end_date));

        return Excel::download(new VehicleExport($startDate, $endDate), 'report.xlsx');
    }
}
