<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'App\Http\Controllers'], function()
{   
    Route::get('/', 'LoginController@index')->name('home');

    Route::get('/login', 'LoginController@index')->name('login');
    Route::post('/login', 'LoginController@loginAction')->name('login.action');
    
    Route::group(['middleware' => ['auth']], function() {
        Route::get('/logout', 'LogoutController@index')->name('logout');
    });

});

Route::namespace('App\Http\Controllers\Admin')->prefix('admin')->middleware('auth')->group(function () {
        Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');

        Route::get('/update_profile', 'UpdateProfileController@index')->name('admin.update_profile');
        Route::post('/update_profile_action', 'UpdateProfileController@action')->name('admin.update_profile_action');

    // ACL
    Route::name('acl.')->prefix('acl')->group(function () {

        Route::get('users', 'Acl\UsersController@index')->name('users.index');
        Route::get('users/userJson', 'Acl\UsersController@userJson')->name('users.userJson');
        Route::post('users/addform', 'Acl\UsersController@addform')->name('users.addform');
        Route::post('users/userPost', 'Acl\UsersController@userPost')->name('users.userPost');
        Route::post('users/editform', 'Acl\UsersController@editform')->name('users.editform');
        Route::delete('users/delete', 'Acl\UsersController@delete')->name('users.delete');

        Route::get('role', 'Acl\RoleController@index')->name('role.index');
        Route::get('role/roleJson', 'Acl\RoleController@roleJson')->name('role.roleJson');
    
    });

    // Kendaraan Masuk
    Route::name('kendaraan_masuk.')->prefix('kendaraan_masuk')->group(function () {

        Route::get('/', 'KendaraanMasuk\KendaraanMasukController@index')->name('index');

        Route::post('/kendaraan_masuk_post', 'KendaraanMasuk\KendaraanMasukController@kendaraanMasukPost')->name('kendaraanMasukPost');

        Route::post('/info_kendaraan_code', 'KendaraanMasuk\KendaraanMasukController@infoKendaraanByCode')->name('infoKendaraanCode');
        
        Route::get('/kendaraanTersediaJson', 'KendaraanMasuk\KendaraanMasukController@kendaraanTersediaJson')->name('kendaraanTersediaJson');
    });

    // Kendaraan Keluar
    Route::name('kendaraan_keluar.')->prefix('kendaraan_keluar')->group(function () {
        Route::get('/', 'KendaraanKeluar\KendaraanKeluarController@index')->name('index');

        Route::post('/kendaraan_keluar_post', 'KendaraanKeluar\kendaraanKeluarController@kendaraanKeluarPost')->name('kendaraanKeluarPost');

        Route::post('/info_kendaraan_code', 'KendaraanKeluar\KendaraanKeluarController@infoKendaraanByCode')->name('infoKendaraanCode');
    });

    // Pelaporan
    Route::name('pelaporan.')->prefix('pelaporan')->group(function () {
        Route::get('/', 'Pelaporan\PelaporanController@index')->name('index');

        Route::get('/pelaporan_json', 'Pelaporan\PelaporanController@pelaporanJson')->name('pelaporanJson');

        Route::get('/export', 'Pelaporan\PelaporanController@exportExcel')->name('exportExcel');

        
    });

    


});

