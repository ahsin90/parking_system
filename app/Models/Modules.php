<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    use HasFactory;

    protected $fillable = [
        'parent_id',
        'name',
        'path',
        'icon',
        'sort',
        'is_active'
    ];
}
