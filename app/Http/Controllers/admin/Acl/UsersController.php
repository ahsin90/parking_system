<?php

namespace App\Http\Controllers\admin\Acl;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use DataTables;

class UsersController extends Controller
{
    
    private $mod_path = 'admin/acl/users';

    function index(){
        if( access($this->mod_path, 'read') === false){
            die("Access denied");
        }

        return view('admin.pages.acl.users');
    }

    function userJson(Request $request){
        if( access($this->mod_path, 'read') === false){
            die("Access denied");
        }

        if ($request->ajax()) {
            $data = User::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('created_at', function ($request) {
                    return $request->created_at->format('Y-m-d H:i:s');
                })
                ->editColumn('updated_at', function ($request) {
                    return $request->updated_at->format('Y-m-d H:i:s');
                })
                ->addColumn('action', function($row){
                    $actionBtn = '
                    <a href="javascript:void(0)" onClick="return load_form2(\''.route('acl.users.editform'). '\', \''.$row['id']. '\')"  data-toggle="modal" data-target="#ahsModalMD" class="edit btn btn-warning btn-sm">Edit</a> 

                    <a href="javascript:void(0)" onClick="return hapus(\''.route('acl.users.delete'). '\', \''.$row['id']. '\', `dt_reload`)" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    function addform(){
        if( access($this->mod_path, 'create') === false){
            die("Access denied");
        }

        $title      = 'Tambah';
        $postRoute  = route('acl.users.userPost');
        return view('admin.pages.acl.add_user', ['title' => 'Tambah', 'postRoute' => $postRoute])->render();
    }

    function userPost(Request $request){
        if( access($this->mod_path, 'create') === false){
            die("Access denied");
        }

        $rules['username'] = 'required';
        $rules['email']     = 'required';
        $rules['full_name'] = 'required';
        $rules['role']      = 'required';
        $rules['password']  = 'required';

        if( $request->password )
        {
            $rules['password']  = 'required|min:8|same:password2';
            $rules['password2'] = 'required';
        }

        $messages =[
              'username.required'=>'Username harus diisi',
              'full_name.required' => 'Nama lengkap harus diisi',
              'email.required' => 'Email harus diiisi',
              'password.min' => 'Panjang password minimal 8 karakter',
              'password.same' => 'Verifikasi password harus sama dengan password',
              'password2.required' => 'Verifikasi password harus diisi',
              'role.required' => 'Role password harus diisi'
          ];



        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()){
            return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ));
        }else{

            if( !empty($request->id) ){
                // update
                $user = User::find($request->id);
                $user->name         = $request->full_name;
                $user->username     = $request->username;
                $user->email        = $request->email;
                $user->password     = $request->password;
                $user->role_id      = $request->role;
                $user->updated_at   = now();
            }else{
                // create new record
                $user = new User;
                $user->name         = $request->full_name;
                $user->username     = $request->username;
                $user->email        = $request->email;
                $user->password     = $request->password;
                $user->role_id      = $request->role;
                $user->created_at   = now();
                $user->updated_at   = now();
            }

            if( $user->save() ){
                return response()->json(array('success' => true), 200);
            }
        }
    }

    function editform(Request $request){
        if( access($this->mod_path, 'update') === false){
            die("Access denied");
        }

        $id = $request->id;
        $title      = 'Update';
        $postRoute  = route('acl.users.userPost');

        $user = new User;
        $data = User::find($id);

        return view('admin.pages.acl.add_user', ['title' => $title, 'postRoute' => $postRoute, 'data' => $data] );
    }

    function delete(Request $request){
        if( access($this->mod_path, 'delete') === false){
            die("Access denied");
        }

        $rules['id'] = 'required';

        $messages =['id.required'=>'ID harus diisi'];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()){
            return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ));
        }else{

            $user = User::find($request->id);

            if( $user->delete() ){
                return response()->json(array('success' => true), 200);
            }
        }
    }
}
