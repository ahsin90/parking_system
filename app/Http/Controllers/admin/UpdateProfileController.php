<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class UpdateProfileController extends Controller
{
    function index(){
        return view('admin.pages.update_profile');
    }

    function action(Request $request){

        $rules['username'] = 'required';
        $rules['email']     = 'required';
        $rules['full_name'] = 'required';

        if( $request->password )
        {
            $rules['password']  = 'required|min:8|same:password2';
            $rules['password2'] = 'required';
        }

        $messages =[
              'username.required'=>'Username harus diisi',
              'full_name.required' => 'Nama lengkap harus diisi',
              'email.required' => 'Email harus diiisi',
              'password.min' => 'Panjang password minimal 8 karakter',
              'password.same' => 'Verifikasi password harus sama dengan password',
              'password2.required' => 'Verifikasi password harus diisi'
          ];



        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()){
            return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ));
        }else{
            $user = Auth::user();
            $user->name = $request->full_name;
            $user->updated_at   = now();

            if( $request->password && $request->password2 )
            {
                $user->password =  $request->password;
            }

            if( $user->save() ){
                return response()->json(array('success' => true), 200);
            }
        }

    }
}
