<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	@include('admin.include.head')
</head>
<body class="alt-menu sidebar-noneoverflow">
	<div id="loader"></div>

	@include('admin.layout.header')
	@include('admin.layout.sidebar')

	
	<!--  BEGIN CONTENT AREA  -->
	<div id="content" class="main-content vector-bg">
	    <div class="layout-px-spacing mt-3">

		    <div class="row">
		        @yield('breadcrumb')
		        <div class="col-12 layout-spacing">
		       		<div class="widget widget-one">
	                    <h2 class="primary-color">@yield('title')</h2>
	                    @yield('content')
	                </div>
		        </div>
		    </div>

		</div> 
	</div>
	<!--  END CONTENT AREA  -->

	
	@include('admin.layout.footer')
	@include('admin.include.modal')

	@yield('script')

</body>
</html>