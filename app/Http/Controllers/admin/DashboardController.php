<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Vehicle;

class DashboardController extends Controller
{
    function index(){

      $startDate  = date("Y-m-d", strtotime("-30 days"));
      $endDate    = date("Y-m-d");

      
      // checkin
      $checkin = Vehicle::whereDate('checkin_at', '>=', $startDate)
            ->whereDate('checkin_at', '<=', $endDate)
            ->selectRaw("COUNT(*) total_vehicle, DATE_FORMAT(checkin_at, '%e-%m-%Y') as date")
            ->groupBy('date')
            ->get();

      // checkout
      $checkout = Vehicle::whereDate('checkout_at', '>=', $startDate)
            ->whereDate('checkout_at', '<=', $endDate)
            ->whereNotNull('checkout_at')
            ->selectRaw("COUNT(*) total_vehicle, DATE_FORMAT(checkout_at, '%e-%m-%Y') as date")
            ->groupBy('date')
            ->get();

      // income
      $income = Vehicle::whereDate('checkout_at', '>=', $startDate)
            ->whereDate('checkout_at', '<=', $endDate)
            ->whereNotNull('price')
            ->selectRaw("SUM(price) as income, DATE_FORMAT(checkout_at, '%e-%m-%Y') as date")
            ->groupBy('date')
            ->get();


      $checkin_arr = [];
      $checkin_label = [];
      foreach( $checkin as $ci ){
         $checkin_arr[]    = $ci->total_vehicle;
         $checkin_label[]  = $ci->date;
      }

      $checkout_arr = [];
      $checkout_label = [];
      foreach( $checkout as $co ){
         $checkout_arr[]    = $co->total_vehicle;
         $checkout_label[]   = $co->date;
      }

      $income_arr = [];
      $income_label = [];
      foreach( $income as $ic ){
         $income_arr[]    = $ic->income;
         $income_label[]   = $ic->date;
      }

      $checkinData = json_encode($checkin_arr);
      $checkinLabel = json_encode($checkin_label);

      $checkoutData = json_encode($checkout_arr);
      $checkoutLabel = json_encode($checkout_label);

      $incomeData = json_encode($income_arr);
      $incomeLabel = json_encode($income_label);

      return view('admin.pages.dashboard', [
         'checkinData'     => $checkinData,
         'checkinLabel'    => $checkinLabel,
         'checkoutData'    => $checkoutData,
         'checkoutLabel'   => $checkoutLabel,
         'incomeData'      => $incomeData,
         'incomeLabel'     => $incomeLabel
      ]);
    }
}
