<?php

namespace App\Http\Controllers\admin\KendaraanKeluar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Vehicle;
use DataTables;
use Carbon\Carbon;

class KendaraanKeluarController extends Controller
{
    private $mod_path = 'admin/kendaraan_keluar';

    function index(){
        if( access($this->mod_path, 'read') === false){
            die("Access denied");
        }

        return view('admin.pages.kendaraan_keluar.index');
    }

    function kendaraanKeluarPost(Request $request){

        if( access($this->mod_path, 'create') === false){
            die("Access denied");
        }

        $rules['code'] = 'required';


        $messages =[
              'code.required'=>'Anda belum memasukkan kode barcode'
          ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()){
            return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ));
        }else{

            // find code
            $q = Vehicle::where('code', $request->code)->whereNull('checkout_at');
            if( $q->count() == 1 ){
                $data = $q->first();

                return response()->json(array(
                        'success' => true,
                        'data'    => $data->code
                    ), 200);

            }else{
                return response()->json(array(
                    'success'   => false,
                    'errors'    => array( ['Kode sudah tidak berlaku'] )
                    )
                );
            }
        }
    }

    function infoKendaraanByCode(Request $request){

        if( access($this->mod_path, 'create') === false){
            die("Access denied");
        }

        $code = $request->code;

        $q = Vehicle::where('code', $code);

        $diffInHours = null;
        $price = 0;
        if( $q->count() == 1 ){
            $data = $q->first();

            // count parking price
            $countHours = Carbon::parse(Carbon::now())->diffInHours($data->checkin_at);
            $totalHours = ($countHours > 0)?$countHours:1;
            
            $countPrice = settings('price_per_hour')*$countHours;
            $price      = ($countPrice >0 )?$countPrice:settings('price_per_hour');

            // update checkout
            $vehicle = Vehicle::find($data->id);
            $vehicle->price  = $price;
            $vehicle->checkout_at  = now();
            $vehicle->updated_at   = now();

            if( $vehicle->save() ){
                
            }

        }else{
            $data = null;
        }

        return view('admin.pages.kendaraan_keluar.info', 
        [
            'data'          => $data, 
            'totalHours'   => $totalHours,
            'price'         => rupiah($price)
        ])->render();
    }
}
