@extends('admin.layout.main')

@section('title', 'Update Profile')

@section('breadcrumb')
<!-- BEGIN BREADCRUMB -->
<div class="col-12 mb-3">
	<nav class="widget breadcrumb-two" aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item active">
				<a href="">Update Profile</a>
			</li>
			<li class="breadcrumb-item"><a href="javascript:void(0);">&nbsp;</a></li>
		</ol>
	</nav>
</div>
<!-- ./END BREADCRUMB -->
@endsection

@section('content')

<div id="errorMessage"></div>

 <form role="form" enctype="multipart/form-data" accept-charset="utf-8" method="post" action="{{ route('admin.update_profile_action') }}" id="save" onsubmit="save(event, `reload`);">
	@csrf
	

	<div class="form-row row">
	    <div class="form-group col-md-6">
	        <label>Username<span class="text-danger">*</span></label><input type="text" name="username" class="form-control" placeholder="" value="{{ auth()->user()->username }}" data-toggle="tooltip" data-placement="top" title="" readonly required />
	    </div>
	    <div class="form-group col-md-6">
	        <label>Email<span class="text-danger">*</span></label><input type="text" name="email" class="form-control" placeholder="" value="{{ auth()->user()->email }}" data-toggle="tooltip" data-placement="top" title="" readonly required />
	    </div>
	    <div class="form-group col-md-12">
	        <label>Nama Lengkap<span class="text-danger">*</span></label><input type="text" name="full_name" class="form-control" placeholder="" value="{{ auth()->user()->name }}" data-toggle="tooltip" data-placement="top" title=""  />
	    </div>
	    <div class="form-group col-md-6">
	        <label>Password</label><input type="password" name="password" class="form-control" placeholder="Diisi jika ingin mengganti password" value="" data-toggle="tooltip" data-placement="top" title="" />
	    </div>
	    <div class="form-group col-md-6">
	        <label>Verifikasi Password</label><input type="password" name="password2" class="form-control" placeholder="Diisi jika ingin mengganti password" value="" data-toggle="tooltip" data-placement="top" title="" />
	    </div>
	    
	    <button type="submit" class="btn btn-primary">Simpan</button>
	</div>
</form>
@endsection

